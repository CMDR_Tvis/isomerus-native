#include <stdexcept>
#include <string>
#include <algorithm>
#include <iostream>
#include "include/atom_stack.hpp"

namespace isomerus_api {
    auto atom_stack::from_string(const std::string &formula) -> std::vector<atom_stack *> * {
      auto string = std::string(formula);
      auto new_list = new std::vector<atom_stack *>();

      while (true) {
        for (const atom_type *i_type:atom_type::get_values()) {
          auto symbol = i_type->get_symbol();

          if (std::string(string).find(symbol) == 0) {
            string = string.substr(symbol.size());
            std::string index("");

            for (char it:string) {
              std::cout << it << std::endl;

              if (std::isdigit(it))
                index += it;

              if (std::isalpha(it))
                break;
            }

            string = string.substr(index.size());
            int index_integer;

            if (index == std::string(""))
              index_integer = 1;
            else
              try {
                index_integer = std::stoi(index);
              } catch (const std::invalid_argument &e) {
                return nullptr;
              }

            new_list->push_back(new atom_stack(*i_type, index_integer));
          }
        }

        if (string.empty())
          break;
      }

      return new_list;
    }

    atom_stack::atom_stack(const atom_type &type, const int quantity) : type(type), quantity(quantity) {}

    const atom_type &atom_stack::get_type() const {
      return type;
    }

    auto atom_stack::get_quantity() const -> int {
      return quantity;
    }

    auto atom_stack::set_quantity(const int value) -> void {
      quantity = value;
    }

    auto atom_stack::to_string() const -> const std::string {
      return std::string("atom_stack(type=") + type.to_string() + ",quantity=" + std::to_string(quantity) + ")";
    }
}
