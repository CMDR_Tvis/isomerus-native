#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <chrono>
#include "include/isomerus.hpp"
#include "include/atom.hpp"
#include "include/atom_stack.hpp"
#include "include/lists.hpp"

namespace isomerus_api {

    auto isomerus_api::isomerus::convert(const std::string &formula) -> isomerus_api::isomerus_output * {
      auto begin = std::chrono::steady_clock::now();
      auto initial_atoms = *new std::vector<isomerus_api::atom *>();
      auto id = 0;
      const auto stacks = isomerus_api::atom_stack::from_string(formula);

      if (!stacks)
        return nullptr;

      std::for_each(stacks->begin(), stacks->end(), [&id, &initial_atoms](isomerus_api::atom_stack *stack) {
          for (auto i = 0; i < stack->get_quantity(); i++) {
            initial_atoms.push_back(new isomerus_api::atom(id, stack->get_type()));
            id++;
          }
      });

      initial_atoms[0]->set_number_in_molecule(1);
      iterate_bindings(initial_atoms, 1);
      auto end = std::chrono::steady_clock::now();

      logger.log(
        std::string("Time elapsed: ") +
        std::to_string(std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count()) +
        "µs.");

      std::for_each(founds.begin(), founds.end(), [*this](std::vector<isomerus_api::atom *> *it) {
          logger.log(isomerus_api::as_formula_string(*it));
      });

      return new isomerus_output(formula, founds);
    }

    auto isomerus_api::isomerus::iterate_bindings(std::vector<isomerus_api::atom *> &atoms,
                                                  unsigned int level) -> bool {

      const unsigned int index = level - 1;

      if (nodes.size() >= level) {
        auto notLast = std::find(nodes[index]->begin(),
                                 nodes[index]->end(),
                                 isomerus_api::as_formula_string(atoms)) != nodes[index]->end();

        if (nodes.size() >= level && notLast) {
          return false;
        } else if (nodes.size() < level) {
          auto new_vector = new std::vector<std::string>();
          new_vector->push_back(isomerus_api::as_formula_string(atoms));
          nodes.insert(nodes.begin() + index, new_vector);
        } else
          nodes[index]->push_back(isomerus_api::as_formula_string(atoms));
      }

      auto do_continue = false;

      std::for_each(atoms.begin(), atoms.end(), [&do_continue](isomerus_api::atom *i) mutable {
          if (i->can_bind())
            do_continue = true;
      });

      if (!do_continue) {
        if (!isomerus_api::contains_deep(founds, atoms))
          founds.push_back(&atoms);

        return true;
      }

      for (unsigned long long i = 0; i < atoms.size(); i++) {
        auto atom1 = atoms[i];

        if (atom1->get_number_in_molecule() == 0 || !atom1->can_bind())
          continue;

        for (unsigned long long j = 0; j < atoms.size(); j++) {
          auto atom2 = atoms[j];

          if (!atom2->can_bind() || i == j)
            continue;

          std::vector<isomerus_api::atom *> &atoms_copy = *new std::vector<isomerus_api::atom *>();

          std::for_each(atoms.begin(), atoms.end(), [&atoms_copy](isomerus_api::atom *atom) mutable {
              atoms_copy.push_back(&atom->copy());
          });

          if (atoms_copy[atom1->get_id()]->bind(*atoms_copy[atom2->get_id()])) {
            atoms_copy
              .at(std::distance(atoms.begin(), std::find(atoms.begin(), atoms.end(), atom2)))
              ->set_number_in_molecule(level);

            iterate_bindings(atoms_copy, level + 1);
          }
        }
      }

      return false;
    }

    isomerus::isomerus(const isomerus_api::isomerus_logger &logger) : logger(logger) {}
}
