#include "include/atom_type.hpp"

#include <array>
#include <utility>

namespace isomerus_api {
    atom_type::atom_type(const unsigned int ordinal, const unsigned int valence, const std::string &symbol) :
      ordinal(ordinal),
      valence(valence),
      symbol(std::move(symbol)) {}

    auto atom_type::get_valence() const -> const int {
      return valence;
    }

    auto atom_type::get_symbol() const -> const std::string {
      return symbol;
    }

    const atom_type &atom_type::carbon = *new atom_type(0, 4, std::string("C"));
    const atom_type &atom_type::hydrogen = *new atom_type(1, 1, std::string("H"));
    const atom_type &atom_type::oxygen = *new atom_type(2, 2, std::string("O"));
    const std::array<const atom_type *, 3> &atom_type::values{{&carbon, &hydrogen, &oxygen}};

    const atom_type &atom_type::get_carbon() {
      return carbon;
    }

    const atom_type &atom_type::get_hydrogen() {
      return hydrogen;
    }

    const atom_type &atom_type::get_oxygen() {
      return oxygen;
    }

    auto atom_type::get_values() -> const std::array<const atom_type *, 3> & {
      return values;
    }

    bool atom_type::operator==(const atom_type &rhs) const {
      return ordinal == rhs.ordinal;
    }

    bool atom_type::operator!=(const atom_type &rhs) const {
      return !(rhs == *this);
    }

    auto atom_type::to_string() const -> const std::string {
      return std::string("atom_type(ordinal=") +
             std::to_string(ordinal) +
             ",valence=" + std::to_string(valence) +
             ",symbol=" + symbol + ")";
    }
}
