#include "include/isomerus_output.hpp"

namespace isomerus_api {
    auto isomerus_output::get_initial_formula() -> const std::string {
      return initial_formula;
    }

    auto isomerus_output::get_results() -> const std::vector<std::vector<isomerus_api::atom *> *> & {
      return results;
    }

    isomerus_output::isomerus_output(const std::string &initialFormula,
                                     const std::vector<std::vector<isomerus_api::atom *> *> &results)
      : initial_formula(initialFormula), results(results) {}
}
