#include <algorithm>
#include <string>
#include <sstream>
#include "include/lists.hpp"

namespace isomerus_api {
    auto as_formula_string(std::vector<isomerus_api::atom *> &atoms) -> const std::string {
      auto tail0 = std::stringstream();

      std::for_each(atoms.begin(), atoms.end(), [&tail0](atom *i_atom) mutable {
          auto tail1 = std::stringstream();
          auto &bonds_vector = *new std::vector<int>(i_atom->get_bonds());
          std::sort(bonds_vector.begin(), bonds_vector.end());

          std::for_each(bonds_vector.begin(), bonds_vector.end(), [&tail1](int bond) mutable {
              if (!tail1.str().empty())
                tail1 << ',';

              tail1 << std::to_string(bond);
          });

          tail0 << i_atom->get_type().get_symbol() + "[" + tail1.str() + "]";
      });

      return tail0.str();
    }

    auto contains_deep(std::vector<std::vector<isomerus_api::atom *> *> &parent,
                       std::vector<isomerus_api::atom *> &list) -> bool {

      auto string = as_formula_string(list);

      for (auto it : parent)
        if (as_formula_string(*it) == string)
          return true;

      return false;
    }
}
