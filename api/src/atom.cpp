#include <tuple>
#include "include/atom.hpp"
#include <algorithm>
#include <sstream>
#include <iostream>

namespace isomerus_api {
    auto isomerus_api::atom::get_bonds() const -> std::vector<int> & {
      return bonds;
    }

    auto isomerus_api::atom::set_bonds(std::vector<int> &value) -> void {
      this->bonds = value;
    }

    auto isomerus_api::atom::get_id() const -> const int {
      return id;
    }

    auto isomerus_api::atom::get_number_in_molecule() const -> int {
      return number_in_molecule;
    }

    auto isomerus_api::atom::set_number_in_molecule(const int value) -> void {
      number_in_molecule = value;
    }

    auto isomerus_api::atom::get_valence() const -> const int {
      return type.get_valence();
    }

    auto isomerus_api::atom::bind(valenceable &other) -> bool {
      if (other.get_bonds().size() + 1 <= other.get_valence() && bonds.size() + 1 <= get_valence()) {
        other.get_bonds().push_back(get_id());
        bonds.push_back(other.get_id());
        return true;
      }

      return false;
    }

    auto isomerus_api::atom::can_bind() const -> bool {
      return get_valence() - bonds.size() > 0;
    }

    auto isomerus_api::atom::operator==(isomerus_api::atom &rhs) -> bool {
      return std::tie(id, type) == std::tie(rhs.id, rhs.type);
    }

    auto isomerus_api::atom::operator!=(isomerus_api::atom &rhs) -> bool {
      return !(rhs == *this);
    }

    auto isomerus_api::atom::copy() -> isomerus_api::atom & {
      const auto new_atom = new isomerus_api::atom(id, type);
      new_atom->number_in_molecule = number_in_molecule;
      new_atom->bonds = *new std::vector<int>(bonds);
      return *new_atom;
    }

    isomerus_api::atom::atom(const int id, const atom_type &type) : id(id), type(type) {}

    auto isomerus_api::atom::get_type() -> isomerus_api::atom_type {
      return type;
    }

    auto atom::to_string() const -> const std::string {
      auto bonds_s = std::stringstream();
      auto it = bonds.begin();

      if (it == bonds.end())
        bonds_s << "[]";
      else {
        bonds_s << '[';

        while (true) {
          bonds_s << *it;
          auto next = std::next(it);

          if (next == bonds.end()) {
            bonds_s << ']';
            break;
          }

          bonds_s << ',';
          it = next;
        }
      }

//
//
//
//      for (unsigned long long index = 0; index < bonds.size(); index++) {
//        auto it = bonds[index];
//        bonds_s << std::to_string(it);
//
//        if (index != (bonds.size() - 1L))
//          bonds_s << ", ";
//      }
//
//      bonds_s << ']';

      return std::string("atom(id=") +
             std::to_string(id) +
             ",type=" +
             type.to_string() +
             ",number_in_molecule=" +
             std::to_string(number_in_molecule) +
             ",bonds=" +
             bonds_s.str() +
             ")";
    }
}
