#pragma once
#ifndef ISOMERUS_NATIVE_ATOM_STACK_HPP
#define ISOMERUS_NATIVE_ATOM_STACK_HPP

#include <list>
#include <string>
#include "atom_type.hpp"

namespace isomerus_api {
    class atom_stack final {
        const isomerus_api::atom_type type;
        int quantity;
    public:
        auto get_quantity() const -> int;

        auto set_quantity(const int value) -> void;

        auto to_string() const -> const std::string;

        auto get_type() const -> const isomerus_api::atom_type &;

        atom_stack(const isomerus_api::atom_type &type, const int quantity);

        static auto from_string(const std::string &formula) -> std::vector<isomerus_api::atom_stack *> *;
    };
}


#endif //ISOMERUS_NATIVE_ATOM_STACK_HPP
