#ifndef ISOMERUS_NATIVE_ISOMERUS_LOGGER_HPP
#define ISOMERUS_NATIVE_ISOMERUS_LOGGER_HPP

#include <string>

namespace isomerus_api {
    class isomerus_logger {
    public:
        virtual auto log(const std::string &message) const -> void = 0;
    };
}

#endif //ISOMERUS_NATIVE_ISOMERUS_LOGGER_HPP
