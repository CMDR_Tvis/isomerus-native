#pragma once
#ifndef ISOMERUS_NATIVE_ATOM_TYPE_HPP
#define ISOMERUS_NATIVE_ATOM_TYPE_HPP

#include <array>
#include <string>
#include <vector>

namespace isomerus_api {
    class atom_type final {
        atom_type(unsigned const int ordinal, unsigned const int valence, const std::string &symbol);

        const unsigned int ordinal;
        const unsigned int valence;
        const std::string symbol;
        static const atom_type &hydrogen;
        static const atom_type &oxygen;
        static const atom_type &carbon;
        static const std::array<const atom_type *, 3> &values;
    public:

        auto get_valence() const -> const int;

        auto get_symbol() const -> const std::string;

        static const atom_type &get_carbon();

        static const atom_type &get_hydrogen();

        static const atom_type &get_oxygen();

        auto to_string() const -> const std::string;

        static auto get_values() -> const std::array<const atom_type *, 3> &;

        auto operator==(const atom_type &rhs) const -> bool;

        auto operator!=(const atom_type &rhs) const -> bool;
    };
}

#endif //ISOMERUS_NATIVE_ATOM_TYPE_HPP
