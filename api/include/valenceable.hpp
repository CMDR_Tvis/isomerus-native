#ifndef ISOMERUS_NATIVE_VALENCEABLE_HPP
#define ISOMERUS_NATIVE_VALENCEABLE_HPP

#include <list>
#include <vector>

namespace isomerus_api {
    class valenceable {
    public:
        virtual auto get_bonds() const -> std::vector<int> & = 0;

        virtual auto set_bonds(std::vector<int> &value) -> void = 0;

        virtual auto get_id() const -> int const = 0;

        virtual auto get_number_in_molecule() const -> int = 0;

        virtual auto set_number_in_molecule(int value) -> void = 0;

        virtual auto get_valence() const -> const int = 0;

        virtual auto bind(isomerus_api::valenceable &other) -> bool = 0;

        virtual auto can_bind() const -> bool = 0;
    };
}

#endif //ISOMERUS_NATIVE_VALENCEABLE_HPP
