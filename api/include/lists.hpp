#pragma once
#ifndef ISOMERUS_NATIVE_LISTS_HPP
#define ISOMERUS_NATIVE_LISTS_HPP

#include <list>
#include <string>
#include <vector>
#include "atom.hpp"

namespace isomerus_api {
    auto as_formula_string(std::vector<isomerus_api::atom *> &atoms) -> const std::string;

    auto contains_deep(std::vector<std::vector<isomerus_api::atom *> *> &parent,
                       std::vector<isomerus_api::atom *> &list) -> bool;
}

#endif //ISOMERUS_NATIVE_LISTS_HPP
