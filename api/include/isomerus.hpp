#pragma once
#ifndef ISOMERUS_NATIVE_ISOMERUS_HPP
#define ISOMERUS_NATIVE_ISOMERUS_HPP

#include <vector>
#include "atom.hpp"
#include "isomerus_output.hpp"
#include "isomerus_logger.hpp"

namespace isomerus_api {
    class isomerus {
        const isomerus_api::isomerus_logger &logger;

        std::vector<std::vector<isomerus_api::atom *> *> &founds =
          *new std::vector<std::vector<isomerus_api::atom *> *>();

        std::vector<std::vector<std::string> *> &nodes = *new std::vector<std::vector<std::string> *>();

        auto iterate_bindings(std::vector<isomerus_api::atom *> &atoms, unsigned int level) -> bool;

    public:
        isomerus(const isomerus_logger &logger);

        auto convert(const std::string &formula) -> isomerus_api::isomerus_output *;
    };
}


#endif //ISOMERUS_NATIVE_ISOMERUS_HPP
