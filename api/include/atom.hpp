#pragma once
#ifndef ISOMERUS_NATIVE_ATOM_HPP
#define ISOMERUS_NATIVE_ATOM_HPP

#include <vector>
#include "valenceable.hpp"
#include "atom_type.hpp"

namespace isomerus_api {
    class atom final : public valenceable {
        const int id;
        const atom_type &type;
        int number_in_molecule = 0;
        std::vector<int> &bonds = *new std::vector<int>();
    public:
        auto get_type() -> isomerus_api::atom_type;

        atom(const int id, const isomerus_api::atom_type &type);

        auto get_bonds() const -> std::vector<int> & override final;

        auto set_bonds(std::vector<int> &value) -> void override final;

        auto get_id() const -> const int override final;

        auto get_number_in_molecule() const -> int override final;

        auto set_number_in_molecule(int value) -> void override final;

        auto get_valence() const -> const int override final;

        auto bind(isomerus_api::valenceable &other) -> bool override final;

        auto can_bind() const -> bool override final;

        auto to_string() const -> const std::string;

        auto operator==(isomerus_api::atom &rhs) -> bool;

        auto operator!=(isomerus_api::atom &rhs) -> bool;

        auto copy() -> isomerus_api::atom &;
    };
}

#endif //ISOMERUS_NATIVE_ATOM_HPP
