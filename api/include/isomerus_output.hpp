#pragma once
#ifndef ISOMERUS_NATIVE_ISOMERUS_OUTPUT_HPP
#define ISOMERUS_NATIVE_ISOMERUS_OUTPUT_HPP

#include "include/atom.hpp"
#include <vector>

namespace isomerus_api {
    class isomerus_output final {
        const std::string &initial_formula;
        const std::vector<std::vector<atom *> *> &results;
    public:
        isomerus_output(const std::string &initialFormula,
                        const std::vector<std::vector<isomerus_api::atom *> *> &results);

        auto get_initial_formula() -> const std::string;

        auto get_results() -> const std::vector<std::vector<isomerus_api::atom *> *> &;
    };
}

#endif //ISOMERUS_NATIVE_ISOMERUS_OUTPUT_HPP
