#include "include/isomerus.hpp"
#include <string>
#include <vector>
#include <include/std_out_logger.hpp>

auto main(int argc, char *argv[]) -> int {
  if (argc >= 2) {
    std::string formula = argv[1];
    (new isomerus_api::isomerus(*new std_out_logger()))->convert(formula);
  }

  return 0;
}
