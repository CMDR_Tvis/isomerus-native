#include "include/std_out_logger.hpp"
#include <string>
#include <iostream>

auto std_out_logger::log(const std::string &message) const -> void {
  std::cout << message << std::endl;
}
