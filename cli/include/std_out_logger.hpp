#ifndef ISOMERUS_NATIVE_STD_OUT_LOGGER_HPP
#define ISOMERUS_NATIVE_STD_OUT_LOGGER_HPP

#include <string>
#include "include/isomerus_logger.hpp"

class std_out_logger : public isomerus_api::isomerus_logger {
public:
    auto log(const std::string &message) const -> void override final;
};


#endif //ISOMERUS_NATIVE_STD_OUT_LOGGER_HPP
